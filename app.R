library(shinythemes)
library(shiny)
library(ggplot2)
library(shinyWidgets)
library(leaflet)
library(scales)

#Rotina de processamento de dados
#source("dados/processamento/processamento.r", local=TRUE)

#Carregar dados já processados
#source("load_dfs.R", local=TRUE)

ui <- fluidPage(theme = shinytheme("flatly"),
  navbarPage(title="BDTD",
             tabPanel(title="Análises básicas",h3("BDTD 2018"), align="center",
                sidebarPanel(
                        selectInput("analise", "Tipo de análise:",
                                  c("Selecione o tipo de análise" = "vazio",
                                    "Quantidade de documentos" = "pub_ano_tipo",
                                    "Mapa" = "mapa",
                                    "Instituições" = "instituicoes")),
                conditionalPanel(
                          condition = "input.analise == 'pub_ano_tipo'",
                          sliderInput("ano_slider", 
                                      label = h5("Definir período"), 
                                      min = 1956, 
                                      max = 2018, 
                                      value = c(1998, 2018), sep=""),
                          conditionalPanel(condition = "input.analise == 'pub_ano_tipo'",
                                           sliderInput('ano_slice',
                                                       h5('Agrupar por intervalo'), min = 1, max = 10,
                                                       value = 5, step = 1),
                                           checkboxGroupInput("tipo","Tipo de documento",
                                                              choices=c("Mestrado" = "masterThesis",
                                                                        "Doutorado" ="doctoralThesis"), 
                                                              selected = c("masterThesis","doctoralThesis"))),
                          selectInput("qtde_regiao","Visualizar por região?",
                                      c("Não" ="nao",
                                        "Sim" = "sim"),
                                      selected = "nao"),
                          conditionalPanel(
                                           condition="input.qtde_regiao == 'sim'",
            
                                           pickerInput("regiao","Região",
                                                       selected = c("NORTE", "NORDESTE", "CENTRO-OESTE","SUDESTE","SUL"),
                                                       choices=c("NORTE", "NORDESTE", "CENTRO-OESTE","SUDESTE","SUL"), 
                                                       options = list(`actions-box` = TRUE,
                                                                      `deselect-all-text` = "Desmarcar todas",
                                                                      `select-all-text` = "Marcar todas",
                                                                      `none-selected-text` = "Selecionar"),multiple = T)),
                          radioButtons(inputId="barras_linhas", label="Tipo de visualização", 
                                       choices=c("Barras" = "barras",
                                                 "Linhas / Pontos" ="linhas"))
                        ),
                        conditionalPanel(condition="input.analise == 'mapa'",
                                         selectInput("mapa_regiao_uf","Visualizar por:",
                                                     c("UF" ="uf",
                                                       "Região" = "regiao_mapa"),
                                                     selected = "UF")
                                         )
                                ),
                      mainPanel(
                        conditionalPanel(condition="input.analise == 'pub_ano_tipo' & input.qtde_regiao == 'nao'",
                                    
                                         tabsetPanel(id='teste',
                                                     tabPanel(title="Quantidade de documentos por período",
                                                              plotOutput('pat'),
                                                              
                                          tabsetPanel(id='teste2',
                                                      tabPanel(title="Quantidade de documentos por período - Agrupado",
                                                               plotOutput('pats') 
                                                               ))
                                                              
                                                              )
                                                     )
                                         ),
                        conditionalPanel(condition="input.analise == 'pub_ano_tipo' & input.qtde_regiao == 'sim'",
                                         
                                         tabsetPanel(id="teste3",
                                                     tabPanel(title="Quantidade de documentos por região",
                                                              plotOutput('regiao')
                                                              ),
                                                     tabsetPanel(id="teste4",
                                                                 tabPanel(title="Quantidade de documentos por região - Agrupado",
                                                                          plotOutput('regiao_slice')
                                                                          ))
                                                     
                                                     )),
                        conditionalPanel(condition="input.analise == 'mapa'",
                                         tabsetPanel(id="teste5",
                                                     tabPanel(title="Cartograma",
                                                              leafletOutput("mapa", height = 600, width = 600)))),
                        conditionalPanel(condition="input.analise =='instituicoes'",
                                         tabsetPanel(id="teste6",
                                                     tabPanel(title="Instituições por quantidade de documentos",
                                                              plotOutput('grafico_instituicoes',height = 1000, width = 600))))
                        )
),
tabPanel(
  title='Análises Avançadas',
  h3("Análises Avançadas"),
  
  sidebarPanel(
    selectInput("analise_avancada", 
                "Tipo de análise:",
                 c("Selecione o tipo de análise" = "vazio",
                 "Heatmap" = "heatmap_inst")),
               conditionalPanel(
                 condition = "input.analise_avancada == 'heatmap_inst'",
                 sliderInput("ano_slider_heat", 
                             label = h4("Período"), 
                             min = 1995, 
                             max = 2018, 
                             value = c(1995, 2018), sep=""))
    
  ),
  mainPanel(conditionalPanel(
    condition = "input.analise_avancada == 'heatmap_inst'",
                             plotOutput('heatmap')))
)
))

server <- function(input, output, session) {
  
  source("graficos/grafico_ano_qtde_tipo.R", local=TRUE)
  source("graficos/heatmap.R", local=TRUE)
  ano_qtde_tipo$Ano <- as.numeric(ano_qtde_tipo$Ano)
  
## Função reativa para valores dentro do slider range de 'Ano'
   ano_slide = reactive({
    ano_qtde_tipo_reactive = subset(ano_qtde_tipo, Ano >= input$ano_slider[1] & Ano <= input$ano_slider[2]&Tipo %in% input$tipo)
    #ano_qtde_tipo_reactive = subset(ano_qtde_tipo, )
    })
  
## OUTPUT - QUANTIDADE DE PUBLICAÇÕES POR ANO (E TIPO)
  output$pat <- renderPlot({
    x <- ano_slide()
   # tipo_box_shiny <- tipo_box()
   grafico_ano_tipo <- ggplot(x, aes(x = Ano, y = Quantidade))+
      theme_bw(base_size = 13)+
      theme(axis.text.x = element_text(angle=65, vjust=0.6),legend.position="bottom") + 
      labs(title="", subtitle="",
           x="Ano de defesa")+
      scale_x_continuous(breaks=c(min(x$Ano,na.rm=TRUE):max(x$Ano,na.rm=TRUE)))+
      scale_y_continuous(label=comma)+
      scale_fill_manual(values=c("#79c36a", "#599ad3"), 
                        name="",
                        breaks=c("masterThesis", "doctoralThesis"),
                        labels=c("Mestrado", "Doutorado"))
     
     
   if (input$barras_linhas=='barras')
   {
     grafico_ano_tipo+
       geom_bar(stat="identity", position='dodge',aes(fill=Tipo), width = 0.85)
     
   }
   else
   {
     grafico_ano_tipo+
       geom_line(aes(color=Tipo),alpha=0.8,size=1.5)+
       #scale_color_manual(values = c('#79c36a','#599ad3')) +
       scale_color_manual(labels = c("Mestrado", "Doutorado"), values = c("#79c36a", "#599ad3"))
       #scale_color_discrete(name = "", labels = c("Mestrado", "Doutorado"))+
       #scale_color_manual(values=c("#79c36a", "#599ad3"))+
   }
   
   
   }, height="auto", width=600)
  
  
## Função Reativa para valores dentro do SLIce ANO
  ano_slice = reactive({
    ano_qtde_tipo = subset(ano_qtde_tipo, Ano >= input$ano_slider[1] & Ano <= input$ano_slider[2]&Tipo %in% input$tipo)  
  ano_qtde_tipo$intervalo <-  cut(ano_qtde_tipo$Ano, seq(from = min(ano_qtde_tipo$Ano,na.rm=TRUE), to = max(ano_qtde_tipo$Ano,na.rm=TRUE), by = input$ano_slice),include.lowest=TRUE, right = FALSE) 
  ano_qtde_tipo <- as.data.frame(ano_qtde_tipo)
  })
  
  ## OUTPUT PUBLICACOES ANO TIPO SLICE
  output$pats <- renderPlot({
    # Histograma de variável categórica
    slice <- ano_slice()
  
    grafico_ano_tipo_slice <- ggplot(slice, aes(x = intervalo, y = Quantidade))+
      theme_bw(base_size = 13)+
      theme(axis.text.x = element_text(angle=65, vjust=0.6),
            legend.position="bottom") + 
      labs(title="", subtitle="",
           x="Período")+
      scale_y_continuous(label=comma)+
      scale_fill_manual(values=c("#79c36a", "#599ad3"), 
                        name="",
                        breaks=c("masterThesis", "doctoralThesis"),
                        labels=c("Mestrado", "Doutorado"))
    
    if (input$barras_linhas=='barras')
    {
      grafico_ano_tipo_slice+
        geom_bar(stat="identity", position='dodge',aes(fill=Tipo), width = 0.85) 
      
    }
    else {
      grafico_ano_tipo_slice+
        geom_point(aes(colour = Tipo),alpha=0.6,size=3)+
        #scale_color_manual(values=c("#79c36a", "#599ad3"))
      scale_color_manual(labels = c("Mestrado", "Doutorado"), values = c("#79c36a", "#599ad3"))+
        stat_summary(aes(group=factor(Tipo),color=Tipo),fun.y = mean, geom="line", size=0.8)
    }
    }, height="auto", width=600)


output$heatmap <- renderPlot({
  
  #Função reativa 'Heatmap'
  ano_heat = reactive({
    ano_heat_reactive = subset(inst_heat, Ano >= input$ano_slider_heat[1] & Ano <= input$ano_slider_heat[2])})
  
  heat <- ano_heat()
  
  ggplot(heat, aes(Ano, Instituicao))+theme_bw(base_size = 13)+
    geom_tile(aes(fill=freq), colour="white")+
    scale_fill_gradient(low = 'steelblue', high = 'red', space = 'Lab', na.value = 'white')+
    scale_x_continuous(breaks=c(1995:2018))+
    theme(axis.text.x = element_text(vjust = 0.5, angle=75))+
    labs(title = "Heatmap de publicações por ano\n",
         x = "Ano de defesa",
         y = "Nome da Instituição")
}, height="auto", width=600)

######
## OUTPUT MAPA

output$mapa <- renderLeaflet({
 # source("graficos/mapas.R")
  mapa_brasil_uf
})

## OUTPUT TODAS AS REGIOES

output$regiao <- renderPlot({
  regioes_todas <- c("NORTE","SUL","LESTE","OESTE","CENTRO-OESTE")
  #Função reativa 'REGIAO'
  bdtd_regiao = reactive({
    bdtd_regiao_reactive = subset(bdtd_uf, Regiao == input$regiao & Ano >= input$ano_slider[1] & Ano <= input$ano_slider[2]&Tipo %in% input$tipo)})
  regiao <- bdtd_regiao()
  
    barras_reactive <- reactive({ 
      if (input$barras_linhas=="barras") {
        +geom_bar()
        +stat_cou
      }
    })
  
  
 grafico_regiao_barras <- ggplot(regiao, aes(x=Ano,fill=Regiao))+
    theme_bw(base_size = 13)+
    theme(axis.text.x = element_text(angle=65, vjust=0.6)) + 
    labs(title="", 
         subtitle="",
         x="Ano de defesa",
         y="Quantidade")+
    scale_x_continuous(breaks=c(min(x$Ano,na.rm=TRUE):max(x$Ano,na.rm=TRUE)))+
    scale_y_continuous(label=comma)

  
  if (input$barras_linhas=="barras") {
    grafico_regiao_barras+
      geom_bar()+
      stat_count()
  }
 
 else{
   grafico_regiao_barras+
     geom_line(stat='count',aes(color=Regiao),alpha=0.8,size=1.5)
 }
  
})


#Reactive
bdtd_regiao = reactive({
  #ano_qtde_tipo$intervalo <-  cut(ano_qtde_tipo$Ano, seq(from = min(ano_qtde_tipo$Ano), to = max(ano_qtde_tipo$Ano), by = input$ano_slice)) 
  #ano_qtde_tipo <- as.data.frame(ano_qtde_tipo)
  #ano_qtde_tipo <- subset(ano_qtde_tipo, Tipo %in% input$tipo)
  bdtd_uf$intervalo <-  cut(bdtd_uf$Ano, seq(from = min(bdtd_uf$Ano), to = max(bdtd_uf$Ano), by = input$ano_slice), include.lowest = TRUE)
  bdtd_uf <- as.data.frame(bdtd_uf)
  bdtd_uf = subset(bdtd_uf, Regiao == input$regiao & Ano >= input$ano_slider[1] & Ano <= input$ano_slider[2]&Tipo %in% input$tipo)
})


## OUTPUT REGIAO SLICE
output$regiao_slice <- renderPlot({
  
  #Função reativa 'REGIAO'
  regiao <- bdtd_regiao()
  
 plot_regiao_slice <- ggplot(regiao, aes(x=intervalo,fill=Regiao))+
   theme_bw()+
    theme(axis.text.x = element_text(angle=65, vjust=0.6)) + 
    labs(title="", 
         subtitle="",
         x="Período",
         y="Quantidade")+
    scale_y_continuous(label=comma)
 #plot_regiao_slice
    #scale_x_continuous(breaks=c(min(x$Ano):max(x$Ano)))
 if (input$barras_linhas=='barras'){ 
   plot_regiao_slice + 
     geom_bar()+
     stat_count()
 } 
 else{
   plot_regiao_slice+
     geom_point(stat='count',aes(colour = Regiao),alpha=0.8,size=4)+
     geom_line(stat='count',aes(group=factor(Regiao),colour=Regiao),alpha=0.6,size=1.1)
 }
})


output$grafico_instituicoes <- renderPlot({

  ggplot(inst_20, aes(x=reorder(Instituicao,Quantidade),y=Quantidade,fill=Tipo,label=Tipo))+
    geom_bar(stat='identity',position='dodge')+
    coord_flip()+
    labs(title = "\n",
         x = "Sigla da Instituição",
         y = "Quantidade de documentos")+
    scale_fill_manual(values=c("#599ad3","#79c36a"),
                      labels = c("Doutorado","Mestrado"))+
    scale_y_continuous(labels = scales::comma, limits = c(0,50000))+
    guides(fill = guide_legend(title = "Tipo\n"))+
    geom_text(aes(label=comma(Quantidade)), size=3.5,position = position_dodge(1),hjust=-0.2,vjust=0.2)+
    theme_bw(base_size = 13)+
    guides(fill = guide_legend(title = "", title.position = "left"))+
    theme(legend.position="right",axis.text.y = element_text(hjust = 1,size =16))
})

}

shinyApp(ui, server)
