library(jsonlite)
library(plyr)
library(dplyr)
library(ggplot2)
library(scales)

## Definir diret�rio
setwd("C:\\Users\\colab\\Desktop\\IBICT\\09 - Setembro\\13 - Base BDTD")

## Calculando a quantidade total de documentos
qtde_docs <- fromJSON("http://172.26.0.14:8080/solr/biblio/select?indent=on&q=*:*&wt=json")
qtde_docs <- as.numeric(qtde_docs$response$numFound)

## Campos que ser�o importados do SOLR ##
## id
## institution
## publishDate
## format
## author
## dc.contributor.advisor1.fl_str_mv

## Loop para gerar arquivos particionados ##
   for (i in 0:round(qtde_docs/10000)) {
      Sys.sleep(5)
     
## Criando url do JSON
url_solr <- paste("http://172.26.0.14:8080/solr/biblio/select?fl=id,institution,publishDate,format,author,dc.contributor.advisor1.fl_str_mv&indent=on&q=*:*&rows=10000&start=",as.integer((i*10000)),"&wt=json",sep="")

## Importando o arquivo JSON particionado direto da URL
bdtd_solr <- fromJSON(url_solr)

## Selecionado apenas o campo $response$docs
bdtd_solr <- bdtd_solr[["response"]][["docs"]]

## Retirar campo character "NULL" e substituir por NA
bdtd_solr[] <- lapply(bdtd_solr,sub,pattern="NULL",replacement=NA)

## Criando arquivo particionado
arquivo <- paste("dados\\particionados\\bdtd_solr_",i,".csv", sep="")
## Salvar arquivo .csv
write.csv(bdtd_solr, file = arquivo)

## Mensagem de alerta sobre arquivo criado
cat(paste("Arquivo bdtd_solr_",i," criado.\n",sep=""),sep="")
}

## Juntando todos os csv's num mesmo arquivo 'BASE_BDTD'
base_bdtd <-read.csv("dados\\particionados\\bdtd_solr_0.csv",header=TRUE)

for (i in 1:round(qtde_docs/10000)){
 base_bdtd <- rbind(base_bdtd,read.csv(paste("dados\\particionados\\bdtd_solr_",i,".csv",sep=""), header=TRUE))
}

## Salvar arquivo .csv com 'BASE_BDTD'
write.csv(base_bdtd, file = "dados\\base_bdtd.csv")
